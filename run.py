import os
import subprocess
import sys

# Input Google CRD SSH Code
CRD_SSH_Code = input("Google CRD SSH Code:")
username = "runner"
password = "root"

# Create user and set password
os.system(f"useradd -m {username}")
os.system(f"adduser {username} sudo")
os.system(f"echo '{username}:{password}' | sudo chpasswd")
os.system(f"sed -i 's/\/bin\/sh/\/bin\/bash/g' /etc/passwd")

# Set PIN and Autostart
Pin = 123456
Autostart = True

class CRDSetup:
    def __init__(self, runner):
        self.installCRD()
        self.installDesktopEnvironment()
        self.installGoogleChrome()
        self.setupKodeKloud(runner)
        self.finish(runner)

    @staticmethod
    def installCRD():
        subprocess.run(['wget', 'https://dl.google.com/linux/direct/chrome-remote-desktop_current_amd64.deb'])
        subprocess.run(['dpkg', '--install', 'chrome-remote-desktop_current_amd64.deb'])
        subprocess.run(['apt', 'install', '--assume-yes', '--fix-broken'])
        print("Chrome Remote Desktop Installed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    @staticmethod
    def installDesktopEnvironment():
        os.system("export DEBIAN_FRONTEND=noninteractive")
        os.system("apt install --assume-yes xfce4 dbus-x11 xscreensaver")
        os.system("bash -c 'echo \"exec /etc/X11/Xsession /usr/bin/xfce4-session\" > /etc/chrome-remote-desktop-session'")
        os.system("systemctl disable lightdm.service")
        print("XFCE4 Desktop Environment Installed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    @staticmethod
    def installGoogleChrome():
        subprocess.run(["wget", "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"])
        subprocess.run(["dpkg", "--install", "google-chrome-stable_current_amd64.deb"])
        subprocess.run(['apt', 'install', '--assume-yes', '--fix-broken'])
        print("Google Chrome Installed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    @staticmethod
    def setupKodeKloud(runner):
        subprocess.run([sys.executable, "-m", "pip", "install", "-U", "git+https://github.com/debakarr/kodekloud-downloader.git"])
        subprocess.run(['sudo', 'apt', 'install', '-y', 'ffmpeg'])
        print("KodeKloud Downloader Installed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        # kodekloud dl -o . -c kodekloud.com_cookies.txt
        
    @staticmethod
    def finish(runner):
        if Autostart:
            os.makedirs(f"/home/{runner}/.config/autostart", exist_ok=True)
            colab_autostart = """[Desktop Entry]
Type=Application
Name=Colab
Exec=sh -c "sensible-browser"
Comment=Open a web browser at session signin.
X-GNOME-Autostart-enabled=true"""
            with open(f"/home/{runner}/.config/autostart/colab.desktop", "w") as f:
                f.write(colab_autostart)
            os.system(f"chmod +x /home/{runner}/.config/autostart/colab.desktop")
            os.system(f"chown {runner}:{runner} /home/{runner}/.config")

        os.system(f"adduser {runner} chrome-remote-desktop")
        command = f"{CRD_SSH_Code} --pin={Pin}"
        os.system(f"su - {runner} -c '{command}'")
        os.system("service chrome-remote-desktop start")

        print("# # # # # # # # # # # # # # # #")
        print("#                             #")
        print("#      Login PIN: 123456      #")
        print("#      Username: runner       #")
        print("#      Password: root         #")
        print("#                             #")
        print("# # # # # # # # # # # # # # # #")
        while True:
            pass

try:
    if CRD_SSH_Code == "":
        print("Please enter authcode from the given link")
    elif len(str(Pin)) < 6:
        print("Enter a pin more or equal to 6 digits")
    else:
        CRDSetup(username)
except NameError as e:
    print("'username' variable not found, Create a user first")
